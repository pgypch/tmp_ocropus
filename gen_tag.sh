#!/bin/bash 
 
_type="\
*.c \
*.cpp \
*.cc \
*.h \
*.hpp \
*.s \
*.java \
"

for _f in $_type; do
  echo "find * -iname "$_f" -type f"
  find * -iname "$_f" -type f | xargs ctags -a
done
