#!/bin/bash
import sqlite3
import os
import time
import OpenOPC

dbfile='device_number.db'

remotehost='10.223.24.51'
opcserver='Matrikon.OPC.Simulation'
tagname='Random.Int4'
sampletime = 2

db_exist = os.path.exists(dbfile)
db = sqlite3.connect(dbfile)
cur = db.cursor()

#create table first time new database is accessed
if not db_exist:
  sqlstring = 'CREATE TABLE opcdata (id 1, tag AAA, val BBB, stat CC, datetime 130808, atime TEXT)'
  cur.execute(sqlstring)
  db.commit()

while 1>0: # infinite loop kill with Ctrl-C
  opc=OpenOPC.client()
  opc.connect(opcserver,remotehost)
  (opcvalue,opcstat,opctime) = opc.read(tagname)
  opc.close()
  atime = time.asctime() # current computer time as float
  sqlstring = 'INSERT INTO opcdata (id, tag, val, stat, datetime, atime) VALUES(NULL, "%s", "%f", "%s", "%s", "%s")' \
  %(tagname,opcvalue,opcstat,opctime,atime)
  cur.execute(sqlstring)
  db.commit()
  cur.execute('SELECT * FROM opcdata')
  dbvalues = cur.fetchall()
  dblen = len(dbvalues)
  print dbvalues[dblen-1] # print newest value in database
  time.sleep(sampletime)
      # end of while loop

