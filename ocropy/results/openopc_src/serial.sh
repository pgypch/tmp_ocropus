#!/bin/bash

#serial=`cat /home/dreamer/Desktop/ocr/ocropus/ocropy/results/serial.txt`
serial=`cat serial.txt`
model=`cat model.txt`
sensor_type=`cat sensor_type.txt`
range=`cat range.txt`
body_rating=`cat body_rating.txt`
size=`cat size.txt`
body=`cat body.txt`
trim=`cat trim.txt`
echo $serial
echo $model
echo $sensor_type
echo $range
echo $body_rating
echo $size
echo $body
echo $trim


python openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.A $serial
python openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.B $model
python openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.C $sensor_type
python openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.D $range
python openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.E $body_rating
python openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.F $size
python openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.G $body
python openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.H $trim


#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.A $serial
#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.B $model
#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.C $sensor_type
#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.D $range
#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.E $body_rating
#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.F $size
#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.G $body
#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.H $trim

#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.B $serial
#python opc.py -H 192.168.1.6 -s Matrikon.OPC.Simulation -w serial_number 0000
#python opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w serial_number 0000
#python opc.py -H 192.168.1.6  -s Matrikon.OPC.Simulation -r Random.READ4
