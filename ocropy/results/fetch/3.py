#!/usr/bin/python
import glob

list_of_files = glob.glob('./*.txt')
i = 0

with open("fetch/results.txt", "w") as fout:
  for filename in list_of_files:
      data_list = open(filename, "r").readlines() #read all the lines into a list
      fout.writelines(data_list) 
      fout.write('\n')
      i = i+1

print "# of the file: ", i

inFile = open("fetch/results.txt")
outFile = open("fetch/serial.txt", "w")
buffer = []
#serial_number = True
for line in inFile:
  buffer.append(line)
  if line.startswith("124460-1-3"):
    # pick a serial number
    #if serial_number:
    outFile.write("".join(buffer))
    # reset the state
      #serial_number = False
      #buffer = []
    #elif line.startswith(" "):
    #  serial_number = True
inFile.close()
outFile.close()
