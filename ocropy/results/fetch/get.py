#!/usr/bin/python
import glob

list_of_files = glob.glob('./*.txt')
i = 0

with open("fetch/results.txt", "w") as fout:
  for filename in list_of_files:
      data_list = open(filename, "r").readlines() #read all the lines into a list
      fout.write('[')
      fout.writelines(data_list) 
      fout.write(']')
      i = i+1

print "# of the file: ", i
