#!/usr/bin/python

#Read mode
try:
  f = open("010001.txt","r")
  print "Name of the file: ", f.name
  try:
    #string = f.read(20) #read the entire contents of a file at once 
    #line = f.readline() #read one line at a time
    lines = f.readlines() #read all the lines into a list
  finally:
    f.close()
except IOError:
  pass

#write mode
try:
  f = open("results.txt", "w")
  try:
    #f.write('la la la') #write a string to a file
    f.writelines(lines) #write a sequence of strings to a file
  finally:
    f.close()
except IOError:
  pass


#openFile = open('*.txt', "r+")
#writeFile = openFile.write("Test abc")

#readFile = openFile.read()
#print readFile

#openFile.close()
