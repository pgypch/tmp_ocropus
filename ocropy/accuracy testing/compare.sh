#!/bin/bash 

_correct=0
_total=0
_error=0
_unknown=0

for _d in `find * -name *.png`; do
  echo $_d
  let "_total = _total + 1"
  _prefix=`echo $_d | sed 's/\.bin\.png$//g'`
##echo $_prefix
  _suffix_correct=".gt.txt"
  _suffix_output=".txt"
  _suffix_temp="~"

  _correct_file=""
  if [ -f ${_prefix}${_suffix_correct} ]; then
    _correct_file="${_prefix}${_suffix_correct}"
  elif [ -f ${_prefix}${_suffix_correct}${_suffix_temp} ]; then
    _correct_file="${_prefix}${_suffix_correct}${_suffix_temp}"
  fi
  if [ "x$_correct_file" == "x" ] || [ ! -f ${_correct_file} ]; then
    echo "Cannot find correct file for $_d"
    let "_unknown = _unknown + 1"
    continue
  fi

  _output_file=""
  if [ -f ${_prefix}${_suffix_output} ]; then
    _output_file="${_prefix}${_suffix_output}"
  elif [ -f ${_prefix}${_suffix_output}${_suffix_temp} ]; then
    _output_file="${_prefix}${_suffix_output}${_suffix_temp}"
  fi
  if [ "x$_output_file" == "x" ] || [ ! -f ${_output_file} ]; then
    echo "Cannot find output file for $_d"
    let "_unknown = _unknown + 1"
    continue
  fi

  echo "Correct = ${_correct_file}, Ouput = ${_output_file}"
#  echo "diff ${_correct_file} ${_output_file}"
  diff ${_correct_file} ${_output_file} 2>&1 > /dev/null && let "_correct = _correct + 1"
done

let "_error = _total - _correct"
let "_error = _error - _unknown"

echo "Total = ${_total}, Correct = ${_correct}, Error = ${_error}, Unknown = ${_unknown}"
