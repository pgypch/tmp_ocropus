#!/usr/bin/python
import glob
import re

list_of_files = glob.glob('temp/0001/*.txt')
i = 0

with open("all.txt", "w") as fout:
  for filename in list_of_files:
      data_list = open(filename, "r").readlines() #read all the lines into a list
      fout.writelines(data_list) 
      fout.write('\n')
      i = i+1

print "# of the file: ", i

inFile = open("all.txt")
outFile = open("serial.txt", "w")
outFile2 = open("model.txt", "w")
outFile3 = open("sensor_type.txt", "w")
outFile4 = open("range.txt", "w")
outFile5 = open("body_rating.txt", "w")
outFile6 = open("size.txt", "w")
outFile7 = open("body.txt", "w")
outFile8 = open("trim.txt", "w")
content = inFile.read()
content2 = inFile.read()

#1. print content serial 
#m = re.search('([0-9]+-[0-9]+-[0-9]+)', content)
#outFile.write(m.group)
#print "Serial number: ", m.group(0)
m = re.findall("0306488|0306482|124460-1-3", content)
outFile.write(m[0])
print "Serial number: ", m[0]

#2.print content model
m2 = re.findall("648DX1D1NAWA3WK1M5|32.24588", content)
outFile2.write(m2[0])
print "model type: ", m2[0]

#3. print content sensor_type
m3 = re.findall("648-DEV0|648-DEV6", content)
if len(m3) == 0:
  m3 = ['0']
outFile3.write(m3[0])
print "sensor type: ", m3[0]

#4. print content range
m4 = re.findall("DEG", content)
if len(m4) == 0:
  m4 = ['0']
outFile4.write(m4[0])
print "range: ", m4[0]

#5. print content body rating
m5 = re.findall("CL250", content)
if len(m5) == 0:
  m5 = ['0']
outFile5.write(m5[0])
print "body rating: ", m5[0]

#6. print content size
m6 = re.findall("0.5", content)
if len(m6) == 0:
  m6 = ['0']
outFile6.write(m6[0])
print "size: ", m6[0]

#7. print content body
m7 = re.findall("BRONZE", content)
if len(m7) == 0:
  m7 = ['0']
outFile7.write(m7[0])
print "body: ", m7[0]

#8. print content trim
m8 = re.findall("316SST", content)
if len(m8) == 0:
  m8 = ['0']
outFile8.write(m8[0])
print "Trim: ", m8[0]

inFile.close()
outFile.close()
outFile2.close()
outFile3.close()
outFile4.close()
outFile5.close()
outFile6.close()
outFile7.close()
outFile8.close()

#print content
#m = re.search('([0-9]+-[0-9]+-[0-9]+)', content)
#m = re.search('0306482', content)
#outFile.write(m.group(0))
#print "Serial number: ", m.group(0)

