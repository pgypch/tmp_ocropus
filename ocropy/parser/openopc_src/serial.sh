#!/bin/bash

#serial=`cat /home/dreamer/Desktop/ocr/ocropus/ocropy/results/serial.txt`
#serial=`cat /home/dreamer/Desktop/ocr/ocropus/ocropy/serial.txt`
serial=`cat serial.txt`
model=`cat model.txt`
sensor_type=`cat sensor_type.txt`
range=`cat range.txt`
body_rating=`cat body_rating.txt`
size=`cat size.txt`
body=`cat body.txt`
trim=`cat trim.txt`
echo $serial
echo $model
echo $sensor_type
echo $range
echo $body_rating
echo $size
echo $body
echo $trim


python parser/openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.A $serial
python parser/openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.B $model
python parser/openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.C $sensor_type
python parser/openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.D $range
python parser/openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.E $body_rating
python parser/openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.F $size
python parser/openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.G $body
python parser/openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.H $trim

#python parser/openopc_src/opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w 1.B $serial
#python openopc_src/opc.py -H 192.168.1.22 -s Matrikon.OPC.Simulation -w 1.A $serial

#python opc.py -H 192.168.1.6 -s Matrikon.OPC.Simulation -w serial_number 0000

#python opc.py -H 10.223.24.51 -s Matrikon.OPC.Simulation -w serial_number 0000

#python opc.py -H 192.168.1.6  -s Matrikon.OPC.Simulation -r Random.READ4
